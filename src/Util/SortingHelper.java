package Util;
import Sort.Code.MergSort;
import Sort.Code.QuickSort;
import Sort.Code.SelectSort;
public class SortingHelper {
    private SortingHelper(){}
    public static <E extends Comparable<E>> boolean isSorted(E[] arr){
        for(int i = 1; i < arr.length; i ++)
            if(arr[i - 1].compareTo(arr[i]) > 0)
                return false;
        return true;
    }
    public static <E extends Comparable<E>> void sortTest(String sortname, E[] arr){
        long startTime = System.nanoTime();
        if(sortname.equals("SelectionSort"))
            SelectSort.Sort(arr);
        else if(sortname.equals("MergeSort"))
            MergSort.Sort(arr);
        else if (sortname.equals("MergeSort2"))
            MergSort.Sort2(arr);
        else if(sortname.equals("QuickSort"))
            QuickSort.Sort(arr);
        long endTime = System.nanoTime();
        double time = (endTime - startTime) / 1000000000.0;
        if(!SortingHelper.isSorted(arr))
            throw new RuntimeException(sortname + " failed");
        System.out.println(String.format("%s , n = %d : %f s", sortname, arr.length, time));
    }
}
