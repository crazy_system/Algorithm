package Search.Code;
//线性查找
public class LinearSearch {

    private  LinearSearch(){}

    public static <T> int Search(T[] data ,T target){
        for (int i = 0; i < data.length; i++) {
            if(data[i].equals(target))
                return  i;
        }
        return -1;
    }

    public static<T> int Search(T[] data,int boundary,T target){
        for (int i = 0; i < boundary; i++) {
            if (data[i].equals(target)) {
                return i;
            }
        }
        return - 1;
    }

}
