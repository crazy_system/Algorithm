package Search.Code;
public class Test implements Comparable<Test>{
    private    Integer id;
    private    String name;

    public Test(Integer id, String name){
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return  true;
        if(obj == null) return false;
        if(this.getClass() != obj.getClass()) return  false;
        Test other = (Test) obj;
        return this.id.equals(other.id)&&this.name.equals(other.name);
    }

    @Override
    public int compareTo(Test o) {
       return -(this.id - o.id);
    }

    @Override
    public String toString() {
        return String.format("Test (id :   %d   ,   name:   %s );",id,name);
    }
}
