package Container;
import Search.Code.LinearSearch;
import Sort.Code.SelectSort;
public class Array<T> {
    private  T[] data;
    private  Integer size;
    public Array(int capacity){
        data = (T[])new Object[capacity];
        size = 0;
    }
    public Array(){
        this(10);
    }
//返回当前数组的大小
    public int getSize(){
        return size;
    }
    //返回当前类的容量
    public int getCapacity(){
        return  data.length;
    }
    //对数组进行判空
    public boolean isEmpty() {
        return size == 0;
    }
    //在数组的末尾添加数据
    public void  addLast(T e){
        add(size,e);
    }
    //在数组的头部添加数据
    public void addFirst(T e){
        add(0,e);
    }
    //为指定的位置插入元素e
    public void add(int index,T e){
        if(index < 0  || index > size)
            throw new IllegalArgumentException("Add failed. The array subscript is out of bounds");
        if(size== data.length)
            resize(2* data.length);
        for(int i = size - 1; i >= index; i--){
            data[i+1] = data[i];
        }
        data[index] = e;
        size++;
    }

    private void resize(int newSize) {
        T[] newData = (T[])new Object[newSize];
        for(int i=0;i<size; i++){
            newData[i] = data[i];
        }
        data = newData;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append(String.format("Array: size = %d, capacity = %d\n",size,data.length));
        res.append('[');
        for(int i = 0;i<size;i++){
            res.append(data[i]);
            if(i!=size - 1)
                res.append(" , ");
        }
        res.append(']');
        return res.toString();
    }
//返回数组下标为index的元素
    T get(int index){
        if(index < 0 || index >= size)
            throw new IndexOutOfBoundsException("Get failed.  index is illegal.");
        return data[index];
    }

    public T getLast(){
        return get(size - 1);
    }
    public T getFirst(){
        return get(0);
    }
//将数组下标为index的位置更改为e
    void  set(int index,T e){
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Set failed. index is illegal.");
        }
        data[index] = e;
    }
    //查看数组中是否有元素e
    public  boolean contains(T e){
        return LinearSearch.Search(data,size, e) > 0?true:false;
    }

    public int find(T e){
        return LinearSearch.Search(data,size,e);
    }
//删除数组中的指定下标的元素，并将删除元素返回
    public T remove(int index){
        if(index < 0 || index >= size)
            throw new IndexOutOfBoundsException("remove faile,Index is illega");
        T ret = data[index];
        for(int i=index+1;i<size;i++){
            data[i-1] = data[i];
        }
        size--;
        if(size == size/4){
            resize(size/2);
        }
        data[size] = null;
        return ret;
    }
    public T removeFirst(){
        return  remove(0);
    }
    public T removeLast() {
        return remove(size - 1);
    }
    public void removeElement(T e){
        int index = find(e);
        while (index!=-1){
            remove(index);
            index = find(e);
        }
    }
}
