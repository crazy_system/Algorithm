package Container;

public class ArrayQueue<T> implements Queue<T>{
    private Array<T> array;
    public ArrayQueue(){
        array = new Array<T>();
    }
    public ArrayQueue(int capacity){
        array = new Array<T>(capacity);
    }
    public int getCapacity(){
        return array.getCapacity();
    }
    @Override
    public void enQueue(T e) {
        array.addLast(e);
    }

    @Override
    public T Dequeue() {
        return array.removeFirst();
    }

    @Override
    public T getFront() {
        return array.getFirst();
    }

    @Override
    public int getSize() {
        return array.getSize();
    }

    @Override
    public boolean isEmpty() {
        return array.isEmpty();
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append(String.format("ArrayQueue : size = %d , capacity = %d\n",array.getSize(),array.getCapacity()));
        ret.append("FRONT-->[");
        for (int i = 0; i < array.getSize(); i++) {
            ret.append(array.get(i));
            if(i!=array.getSize() - 1)
                ret.append(" , ");
        }
        ret.append("] <--TAIL");
        return ret.toString();
    }
}
