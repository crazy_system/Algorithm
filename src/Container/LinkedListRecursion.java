package Container;

public class LinkedListRecursion<T> {
    private class Node{
        public T e;
        public Node next;
        public Node(T e,Node next){
            this.e = e;
            this.next = next;
        }
        public Node(T e){
            this(e,null);
        }

        @Override
        public String toString() {
            return "Node{" +
                    "e=" + e +
                    ", next=" + next +
                    '}';
        }
    }
    private Node head;
    private int size;
    public LinkedListRecursion() {
        head = null;
        size = 0;
    }
    public int getSize() {
        return size;
    }
    public boolean isEmpty() {
        return (size == 0);
    }
    private Node add(Node node,int index,T e){
        if(index == 0)
            return new Node(e,node);
        node.next = add(node.next,index-1,e);
        return node;
    }
    public void add(int index,T e){
        if(index<0||index>size)
            throw new IndexOutOfBoundsException("Add fileed.Illegal index");
        head = add(head,index,e);
        size++;
    }
    public void addFirst(T e){
        add(0,e);
    }
    public void addLast(T e) {
        add(size,e);
    }
    private T get(Node node,int index){
        if (index == 0)
            return node.e;
        return get(node.next,index-1);
    }
    public T get(int index) {
        if(index<0||index > size)
            throw new IndexOutOfBoundsException("Get failed,Illegal index.");
        return get(head,index);
    }
    public T getFirst(){
        return get(0);
    }
    public T getLast(){
        return get(size - 1);
    }
    private void set(Node node,int index,T e){
        if(index==0){
            node.e = e;
            return ;
        }
        set(node.next,index-1,e);
    }
    public void set(int index,T e){
        if (index < 0 || index > size)
            throw new IndexOutOfBoundsException("Ipdate failed. Illegal index.");
        set(head,index,e);
    }
    private boolean contains(Node node,T e){
        if (node == null)
            return false;
        if(node.e.equals(e))
            return true;
        return contains(node.next,e);
    }
    public boolean contains(T e){
        return contains(head, e);
    }
    //链表的递归删除
    private Node remove(Node node,int index){
        if (index==0){
            Node ret = node;
            node = node.next;
            return ret;
        }
        if(index==1){
            Node ret = node;
        }
        return node;
    }
    private Node removeElement(Node node,T e){
        if(node == null)
            return null;
        node.next = removeElement(node.next,e);
        if (node.e.equals(e)){
            size--;
            return  node.next;
        }
        return node;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Node cur = head;
        while (cur != null) {
            sb.append(cur+" -> ");
            cur = cur.next;
        }
        sb.append("NULL");
        return sb.toString();
    }
}
