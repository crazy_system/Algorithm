package Container;

import Sort.Code.SelectSort;
import Sort.Code.Test;

public class Main {
    public static void main(String[] args) {
        
//        Array<Test> array = new Array<>();
//        array.addFirst(new Test(0,"CAS"));
//        array.addFirst(new Test(2,"sed"));
//        array.addFirst(new Test(2,"enen"));
//        System.out.println(array);
//        System.out.println("remove some node");
//        array.removeElement(new Test(2,"sed"));
//        System.out.println(array);
//
//        ArrayStack<Integer> stack = new ArrayStack<>();
//        for (int i = 0; i <3 ; i++) {
//            stack.Push(i);
//            System.out.println(stack);
//        }
//        stack.Pop();
//        System.out.println(stack);
//
//        LoopQueue<Integer> queue = new LoopQueue<>();
//        for(int i =0;i<3;i++){
//            queue.enQueue(i);
//            System.out.println(queue);
//            if(i%3==2){
//                queue.Dequeue();
//                System.out.println(queue);
//            }
//        }
//        LinkedList<Integer> linkedList = new LinkedList<Integer>();
//        for (int i = 0; i <5;i++ ) {
//            linkedList.add(i,i);
//        }
//        System.out.println(linkedList);
//        linkedList.add(2,0);
//        System.out.println(linkedList);
//        System.out.println(linkedList.removeFirst());
//        System.out.println(linkedList.removeLast());
//        System.out.println(linkedList);
//        LinkedListStack<Integer> listStack = new LinkedListStack<>();
//        for (int i = 0; i <3;i++ ) {
//            listStack.Push(i);
//        }
//        System.out.println(listStack);
//        listStack.Pop();
//        System.out.println(listStack);

        LinkedListQueue<Integer> queue = new LinkedListQueue<>();
        for(int i =0;i<3;i++){
            queue.enQueue(i);
            System.out.println(queue);
            if(i%3==2){
                queue.Dequeue();
                System.out.println(queue);
            }
        }
    }
}
