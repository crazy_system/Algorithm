package Container;

public interface Queue<T>{
    void enQueue(T e);
    T Dequeue();
    T getFront();
    int getSize();
    boolean isEmpty();
}
