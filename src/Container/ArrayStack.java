package Container;
public class ArrayStack<T> implements Stack<T> {
    Array<T> array;
    public ArrayStack() {
        array = new Array<T>();
    }
    public ArrayStack(int capacity){
        array = new Array<T>(capacity);
    }
    public int getCapacity(){
        return array.getCapacity();
    }
    @Override
    public int getSize() {
        return array.getSize();
    }

    @Override
    public boolean isEmpty() {
        return array.isEmpty();
    }

    @Override
    public void Push(T e) {
        array.addLast(e);
    }

    @Override
    public T Pop() {
        return array.removeLast();
    }

    @Override
    public T Peek() {
        return array.getLast();
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append("ArrayStack: ");
        ret.append("[");
        for (int i = 0; i < array.getSize(); i++) {
            ret.append(array.get(i));
            if(i!=array.getSize() - 1)
                ret.append(" , ");
        }
        ret.append("] <--TOP");
        return ret.toString();
    }

}
