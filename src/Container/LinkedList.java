package Container;

public class LinkedList<T> {
    private class Node {
        public T e;
        public Node next;
        public Node(T e,Node next){
            this.e = e;
            this.next = next;
        }
        public Node(T e) {
            this(e,null);
        }
        public Node(){
            this(null,null);
        }

        @Override
        public String toString() {
            return e.toString();
        }
    }
    private Node DummyHead;
    int size;

    public LinkedList() {
        DummyHead = new Node(null,null);
        size = 0;
    }
    public int getSize(){
        return size;
    }
    public boolean isEmpty() {
        return size == 0;
    }
    public void addFirst(T e){
        DummyHead.next = new Node(e,DummyHead.next);
        size++;
    }
    public void add(int index,T e){
        if(index<0 || index > size)
            throw new IndexOutOfBoundsException("Add file. Illegal index ");
        Node prev = DummyHead;
        for(int i = 0;i<index;i++){
            prev = prev.next;
        }
        prev.next = new Node(e, prev.next);
        size++;
    }

    public void addLast(T e){
        add(size,e);
    }

    public T get(int index){
        if(index<0 || index > size)
            throw new IndexOutOfBoundsException("Add file. Illegal index ");
        Node cur = DummyHead.next;
        for (int i = 0; i < index; i++) {
            cur = cur.next;
        }
        return cur.e;
    }

    public T getFirst(){
        return DummyHead.next.e;
    }

    public T getLast(){
        return get(size-1);
    }

    public void set(int index,T e){
        if(index<0 || index > size)
            throw new IndexOutOfBoundsException("Add file. Illegal index ");
        Node cur = DummyHead.next;
        for (int i = 0; i < index; i++) {
            cur = cur.next;
        }
        cur.e = e;
    }

    public boolean contains(T e){
        Node cur = DummyHead.next;
        while (cur != null) {
            if(cur.e.equals(e)){
                return true;
            }
            cur = cur.next;
        }
        return false;
    }

    public T remove(int index){
        if(index<0 || index > size)
            throw new IndexOutOfBoundsException("Add file. Illegal index ");
        Node prev = DummyHead;
        for (int i = 0; i < index; i++) {
            prev = prev.next;
        }
        Node ret = prev.next;
        prev.next = ret.next;
        ret.next = null;
        size--;
        return ret.e;
    }
    public T removeFirst(){
        return remove(0);
    }
    public T removeLast() {
        return remove(size-1);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LinkList: ");
//        Node cur = DummyHead.next;
//        while (cur != null) {
//            sb.append(cur+" -> ");
//            cur = cur.next;
//        }
        for(Node cur = DummyHead.next; cur != null; cur = cur.next)
            sb.append(cur+" -> ");
        sb.append("NULL");
        return sb.toString();
    }
}
