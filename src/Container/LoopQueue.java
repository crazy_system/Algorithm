package Container;

public class LoopQueue<T> implements Queue<T> {
    private T[] data;
    private int front,tail;
    private int size;
    public LoopQueue(int capacity) {
        data = (T[])new Object[capacity+1];
        front = 0;
        tail = 0;
        size = 0;
    }
    public LoopQueue(){
        this(10);
    }
    public int getCapacity(){
        return data.length-1;
    }
    private void resize(int newCapacity){
        T[] newData = (T[])new Object[newCapacity+1];
        for (int i = 0; i < size; i++) {
            newData[i] = data[(i+front)% data.length];
        }
        data = newData;
        front = 0;
        tail = size;
    }
    @Override
    public void enQueue(T e) {
        if(tail+1% data.length==front){
            resize(getCapacity()*2);
        }
        data[tail] = e;
        tail = (tail + 1)%data.length;
        size++;
    }

    @Override
    public T Dequeue() {
        if(isEmpty())
            throw new IllegalStateException("Cannot dequeue from an empty queue");
        T ret = data[front];
        data[front] = null;
        front = (front + 1)%data.length;
        size--;
        if(size == getCapacity()/4&&getCapacity()/2!=0)
            resize(getCapacity()/2);
        return ret;
    }

    @Override
    public T getFront() {
        return data[front];
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return front==tail;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append(String.format("LoopQueue : size = %d , capacity = %d\n",size,getCapacity()));
        ret.append("FRONT-->[");
        for (int i = front; i != tail; i=(i+1)% data.length) {
            ret.append(data[i]);
            if((i+1)% data.length !=tail)
                ret.append(" , ");
        }
        ret.append("] <--TAIL");
        return ret.toString();
    }
}
