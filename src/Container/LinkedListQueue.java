package Container;

public class LinkedListQueue<T> implements Queue<T> {
    private class Node {
        public T e;
        public Node next;
        public Node(T e,Node next){
            this.e = e;
            this.next = next;
        }
        public Node(T e){
            this(e,null);
        }

        @Override
        public String toString() {
            return e.toString();
        }
    }
    private Node head,tail;
    private int size;
    public LinkedListQueue() {
        head = null;
        tail = null;
        size = 0;
    }
    @Override
    public void enQueue(T e) {
        if(tail == null){
            tail = new Node(e);
            head = tail;
        }else{
            tail.next = new Node(e);
            tail = tail.next;
        }
        size++;
    }

    @Override
    public T Dequeue() {
        if(isEmpty())
            throw new IllegalStateException("cannot dequeue from an empty queue.");
        Node ret = head;
        head = head.next;
        ret.next = null;
        if(head==null)
            tail = null;
        size--;
        return ret.e;
    }

    @Override
    public T getFront() {
        if (isEmpty()) {
            throw new IllegalStateException("Queue is empty");
        }
        return head.e;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LinkedListQueue : front ");
        Node cur = head;
        while (cur != null) {
            sb.append(cur+" -> ");
            cur = cur.next;
        }
        sb.append("NUll tail");
        return sb.toString();
    }
}
