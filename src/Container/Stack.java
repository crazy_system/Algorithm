package Container;

public interface Stack <T>{
    int getSize();
    boolean isEmpty();
    void Push(T e);
    T Pop();
    T Peek();
}
