package Container;

public class LinkedListStack<T>  implements Stack<T>{
    private LinkedList<T> list;
    public LinkedListStack() {
        list = new LinkedList<>();
    }
    @Override
    public int getSize() {
        return list.getSize();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public void Push(T e) {
        list.addFirst(e);
    }

    @Override
    public T Pop() {
        return list.removeFirst();
    }

    @Override
    public T Peek() {
        return list.getFirst();
    }

    @Override
    public String toString() {
        return "LinkedListStack{" +
                "list=" + list +
                '}';
    }
}
