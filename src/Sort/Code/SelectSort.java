package Sort.Code;

public class SelectSort {

    private SelectSort(){}
    // 选择出来数组中指定数据区间中的最小数值的下标
    public static <T extends Comparable<T>> int MinIndex(T[] data,int left,int right){
        int minIndex = left;
        for(int i = left;i<=right;i++){
            if(data[i].compareTo(data[minIndex])<0){
                minIndex = i;
            }
        }
        return minIndex;
    }
// 进行数组中指定两个下标的内容进行交换
    public static <T> void swap(T[] data,int i,int j){
        T temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }
    //选择排序
    public static <T extends Comparable<T>> void Sort(T[] data){
        int len = data.length-1;
        for(int i=0;i<=len;i++){
            swap(data,i,MinIndex(data,i,len));
        }
    }

}
