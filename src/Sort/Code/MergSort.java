package Sort.Code;

import Util.ArrayGenerator;
import Util.SortingHelper;

import java.lang.reflect.Array;
import java.util.Arrays;

public class MergSort {
    private MergSort() {
    }
// 归并
    private static <T extends Comparable<T>> void merge(T[] arr, int l, int mid, int r) {
        T[] temp = Arrays.copyOfRange(arr, l, r + 1);
        int i = l, j = mid + 1;
        for (int k = l; k <= r; k++) {
            if (i > mid) {
                arr[k] = temp[j - l];
                j++;
            } else if (j > r) {
                arr[k] = temp[i - l];
                i++;
            } else if (temp[i - l].compareTo(temp[j - l]) <= 0) {
                arr[k] = temp[i - l];
                i++;
            } else {
                arr[k] = temp[j - l];
                j++;
            }
        }
    }
    //递归，分治
    private static <T extends Comparable<T>> void sort(T[] arr,int l,int r){
        if(l >= r) return;
        int mid = l+(r-l) /2;
        sort(arr,l,mid);
        sort(arr,mid+1,r);
        if(arr[mid].compareTo(arr[mid+1])>0)
            merge(arr,l,mid,r);
    }
    //函数调用接口
    public static <T extends Comparable<T>> void  Sort(T[] arr){
        sort(arr,0, arr.length-1);
    }
    private static <T extends Comparable<T>> void merge2(T[] arr, int l, int mid, int r,T[] temp) {
        System.arraycopy(arr,l,temp,l,r-l+1);
        int i = l, j = mid + 1;
        for (int k = l; k <= r; k++) {
            if (i > mid) {
                arr[k] = temp[j];
                j++;
            } else if (j > r) {
                arr[k] = temp[i];
                i++;
            } else if (temp[i].compareTo(temp[j]) <= 0) {
                arr[k] = temp[i];
                i++;
            } else {
                arr[k] = temp[j];
                j++;
            }
        }
    }
    private static <T extends Comparable<T>> void sort2(T[] arr,int l,int r,T[] temp){
        if(r - l <=15){
            InsertSort.Sort2(arr,l,r);
            return;
        }
        int mid = l+(r-l) /2;
        sort2(arr,l,mid,temp);
        sort2(arr,mid+1,r,temp);
        if(arr[mid].compareTo(arr[mid+1])>0)
            merge2(arr,l,mid,r,temp);
    }
    //函数调用接口
    public static <T extends Comparable<T>> void  Sort2(T[] arr){
        T[] temp = Arrays.copyOf(arr,arr.length);
        sort2(arr,0, arr.length-1,temp);
    }
// 自底向上的归并排序
    public static < T extends Comparable<T>> void  SortBU(T[] arr){
        T[] temp = Arrays.copyOf(arr, arr.length);
        int n = arr.length;
        //遍历合并的区间长度
        for(int sz = 1;sz < n; sz+=sz){
            //遍历合并的两个区间的起始位置i
            //合并[i,i+sz-1] 和 [i+sz,i+sz+sz-1]
            for(int i=0;i+sz < n;i+=sz+sz){
                if(arr[i+sz-1].compareTo(arr[i+sz])>0)
                    merge2(arr,i,i+sz-1,Math.min(i+sz+sz-1,n-1),temp);
            }
        }
    }
    public static void main(String[] args) {
        int n = 100000;
        Integer[] arr = ArrayGenerator.generateRandomArray(n,n);
        Integer[] arr1 = Arrays.copyOf(arr,arr.length);
        SortingHelper.sortTest("MergeSort",arr);
//        SortingHelper.sortTest("MergeSort2",arr1);
    }
}
