package Sort.Code;

import Util.ArrayGenerator;
import Util.SortingHelper;

import java.util.Random;

public class QuickSort {
    private QuickSort(){}
    private static <T> void swap(T[] arr,int i,int j){
        T temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
    private static <T extends Comparable<T>> int partition(T[] arr,int l,int r,Random rnd){
        int p = l + rnd.nextInt(r-l +1);
        swap(arr,l,p);
        int j = l;
        for(int i = l+1;i<=r;i++){
            if(arr[i].compareTo(arr[l])<0){
                j++;
                swap(arr,i,j);
            }
        }
        swap(arr,l,j);
        return j;
    }

    private static <T extends Comparable<T>> void sort(T[] arr,int l,int r,Random rnd){
        if(l>= r) return;
        int p = partition(arr,l,r,rnd);
        sort(arr,l,p-1,rnd);
        sort(arr,p+1,r,rnd);
    }
    public static <T extends Comparable<T>> void Sort(T[] arr){
        Random rnd = new Random();
        sort(arr,0,arr.length-1,rnd);
    }
    public static void main(String[] args){
        int n = 100000;
        Integer[] arr = ArrayGenerator.generateRandomArray(n,n);
        SortingHelper.sortTest("QuickSort",arr);
    }
}