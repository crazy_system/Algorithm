package Sort.Code;

public class InsertSort {
    private  InsertSort(){}
    private static <T> void  Swap(T[] dara,int i, int j){
        T temp = dara[i];
        dara[i] = dara[j];
        dara[j] = temp;
    }
    // 找到数据要插入的位置下标
    private static <T extends Comparable<T>> int Suitable(T[] data,int i){
        T temp = data[i];
        for(i = i-1;i>=0;i--){
            if(data[i].compareTo(temp)<0) return i+1;
            else data[i+1] = data[i];
        }
        return 0;
    }
    //发现数据进行交换
    public static <T extends  Comparable<T>> void  Sort1(T[] data){
        for(int i =  0; i<data.length ; i++){
            for(int j = i;j-1>=0&&data[j].compareTo(data[j-1])>0;j--){
                Swap(data, j , j-1);
            }
        }
    }
    //将要插入的数据存储起来，然后将数据后移
    public static <T extends  Comparable<T>> void  Sort2(T[] data){
        for(int i =  0; i<data.length ; i++){
            T temp = data[i];
            data[Suitable(data,i)] = temp;
        }
    }
    public static <T extends  Comparable<T>> void  Sort2(T[] data,int l,int r){
        for(int i =  l; i<r ; i++){
            T temp = data[i];
            data[Suitable(data,i)] = temp;
        }
    }
}
